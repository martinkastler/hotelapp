<?php

$pageTitle = "Impressum";
$metaDesc = "SEO Meta Description";
include("inc/header.php");

?>

<h1>Impressum</h1>

<p><strong>Hotel.ly e.U.</strong><br>
    Inhaber Max Mustermann</p>

<p>Eingetragenes Einzelunternehmen</p>

<p>Hotelbetrieb</p>

<p>UID: ATU12345678</p>

<p>FN: 123456a<br>
    FB-Gericht: Wien<br>
    Sitz: 1030 Wien, Marxergasse 1a, Österreich</p>

<p>Tel: <a href="tel:+431789456">+43 1 78 94 56</a><br>
    E-Mail: <a href="mailto:info@hotel.ly">info@hotel.ly</a></p>

<p>Mitglied der WKÖ</p>

<p>Magistrat 1010 Wien</p>

<p>Verbraucher haben die Möglichkeit Beschwerden an die Online-Streitbeteiligungsplattform der EU zu richten: <a
        href="https://ec.europa.eu/odr">https://ec.europa.eu/odr</a>. Sie können allfällige Beschwerde auch an die oben
    genannte E-Mail-Adresse richten.</p>

<h2>Projektteam</h2>

<div class="row">
    <div class="col-sm-12 col-md-2">
        <img src="images/portrait-placeholder.png" class="img-thumbnail" alt="" role="presentation">
    </div>
    <div class="col-sm-12 col-md-10">
        <p>
            <strong>Max Mustermann</strong><br>
            Lorem ipsum
        </p>    
    </div>
</div>



<?php

include("inc/footer.php")

    ?>