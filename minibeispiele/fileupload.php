<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $invalidFileUploadMessage = false;

    if (!is_uploaded_file($_FILES['uploadfield']['tmp_name']) || !file_exists($_FILES['uploadfield']['tmp_name'])) {

        $invalidFileUploadMessage = "Please select a file";

    } 
    
    // Add more checks using elseif conditions:
    // e.g. check max filesize
    // e.g. check filetypes
    // Recommended reading: https://phppot.com/php/php-image-upload-with-size-type-dimension-validation/
    // Recommended reading: https://www.w3schools.com/php/php_file_upload.asp
    
    else {

        if (is_uploaded_file($_FILES["uploadfield"]["tmp_name"])) {
            $tempFile = $_FILES["uploadfield"]["tmp_name"];
            $targetDirectory = __DIR__ . "/"; // e.g. same location as script
            $targetFile = $targetDirectory.$_FILES["uploadfield"]["name"];

            if (!move_uploaded_file($tempFile, $targetFile)) {
                $invalidFileUploadMessage = "Could not store file";
            } else {
                // Add more file operations
                // e.g. rename file
                // e.g. resize image file with imagescale()
            }
        }
    }
}

?>
<!doctype html>
<html lang="en">
    <head>
        <title>Fileupload</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Example for file upload">
    </head>
    <body>

        <h1>Fileupload</h1>

        <form method="post" enctype="multipart/form-data">

            <div>
                <label for="fileupload">Select file:</label>
            </div>

            <div>
                <input type="file" id="fileupload" name="uploadfield">
                <?= !empty($invalidFileUploadMessage) ? '<div style="color:red">' . $invalidFileUploadMessage . '</div>' : '' ?>
            </div>

            <div>
                <input type="submit" value="Upload">
            </div>

        </form>

    </body>
</html>