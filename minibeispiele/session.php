<?php

session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if(isset($_POST["loginbutton"])) {
        $_SESSION["state"] = "active";
        header("Refresh:0");
    }
    
    if(isset($_POST["logoutbutton"])) {
        session_unset();
        session_destroy();
        header("Refresh:0");
    }

}

?>
<!doctype html>
<html lang="en">
    <head>
        <title>Session</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Example for session">
    </head>
    <body>

        <h1>Session</h1>

        <?php if(!empty($_SESSION["state"]) && $_SESSION["state"] === "active"): ?>

            <form method="post">
                <input type="submit" value="Logout" name="logoutbutton">
            </form>

        <?php else: ?>

            <form method="post">
                <input type="submit" value="Login" name="loginbutton">
            </form>

        <?php endif; ?>

    </body>
</html>