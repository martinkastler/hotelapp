<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if(isset($_POST["setcookiebutton"])) {
        setcookie("mycookie", "Cookie is set", time() + 3600); // 1 hour in the future
        header("Refresh:0");
    }
    
    if(isset($_POST["deletecookiebutton"])) {
        setcookie("mycookie", "", time() - 3600); // 1 hour in the past
        header("Refresh:0");
    }

}

?>
<!doctype html>
<html lang="en">
    <head>
        <title>Cookies</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Example for cookies">
    </head>
    <body>

        <h1>Cookies</h1>

        <p>
            <?php if(!empty($_COOKIE["mycookie"])): ?>
                <?= $_COOKIE["mycookie"] ?>
            <?php else: ?>
                No cookie set
            <?php endif; ?>
        </p>

        <form method="post">
            <input type="submit" value="Set cookie" name="setcookiebutton">
            <input type="submit" value="Delete cookie" name="deletecookiebutton">
        </form>

    </body>
</html>