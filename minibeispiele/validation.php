<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $invalidTextMessage = false;

    if (isset($_POST["textfield"]))
     {
        $textfield = $_POST["textfield"];
        if (strlen($textfield) == 0) {
            $invalidTextMessage = "Please enter a text";
        } elseif (strlen($textfield) < 8) {
            $invalidTextMessage = "Please enter minimum 8 characters text";
        }
     }

}

?>
<!doctype html>
<html lang="en">
    <head>
        <title>Validation</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Example for validation">
    </head>
    <body>

        <h1>Validation</h1>

        <h2>Validation without <code>novalidate</code></h2>
        <form method="post">
            <div>
                <label for="textbox">Text (required):</label>
            </div>
            <div>
                <input type="text" id="textbox" placeholder="min. 8 characters text" name="textfield" minlength="8" required>
                <input type="submit" value="Submit">
                <?= !empty($invalidTextMessage) ? '<div style="color:red">' . $invalidTextMessage . '</div>' : '' ?>
            </div>
        </form>


        <h2>Validation with <code>novalidate</code></h2>
        <form method="post" novalidate>
            <div>
                <label for="textbox">Text (required):</label>
            </div>
            <div>
                <input type="text" id="textbox" name="textfield" placeholder="min. 8 characters text" minlength="8" required>
                <input type="submit" value="Submit">
                <?= !empty($invalidTextMessage) ? '<div style="color:red">' . $invalidTextMessage . '</div>' : '' ?>
            </div>
        </form>

    </body>
</html>