<?php

    require_once("dbaccess.php");

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
        if(isset($_POST["check"]) && isset($_POST["id"]) && isset($_POST["password"])) {
            // TODO
        }


        if(isset($_POST["insert"]) && isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["email"])) {
            // TODO
        }


        if(isset($_POST["update"]) && isset($_POST["username"]) && isset($_POST["email"])) {
            // TODO
        }


        if(isset($_POST["delete"]) && isset($_POST["id"])) {
            // TODO
        }

    }

?>
<!doctype html>
<html lang="en">
    <head>
        <title>Database</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Example for database">
    </head>
    <body>

        <h1>Database</h1>
        <h2><code>SELECT</code></h2>
        <form method="post">
            <input type="text" name="id" placeholder="User ID" required>
            <input type="password" name="password" placeholder="Password" required>
            <input type="submit" name="check" value="Check password">
        </form> 
       

        <h2><code>INSERT INTO</code></h2>
        <form method="post">
            <input type="text" name="username" placeholder="Username" required>
            <input type="password" name="password" placeholder="Password" required>
            <input type="email" name="email" placeholder="Email" required>
            <input type="submit" name="insert" value="Insert">
        </form>


        <h2><code>UPDATE</code></h2>
        <form method="post">
            <input type="text" name="username" placeholder="Username" required>
            <input type="email" name="email" placeholder="New email" required>
            <input type="submit" name="update" value="Update email">
        </form>

        
        <h2><code>DELETE</code></h2>
        <form method="post">
            <input type="text" name="id" placeholder="User ID" required>
            <input type="submit" name="delete" value="Delete">
        </form>


        <hr>

        <h2>Loop over table <code>users</code></h2>
        <?php

            // TODO
        ?>


        <h2>Loop over table <code>news</code></h2>
        <?php

            // TODO

        ?>



        <hr>

        <h2>Filter and loop using <code>JOIN</code></h2>
        <?php

            // TODO

        ?>
                    
    </body>
</html>