<?php

$pageTitle = "Bestes Boutique-Hotel Wien";
$metaDesc = "SEO Meta Description";
include("inc/header.php");

?>

<h1>Willkommen im besten Boutique-Hotel Wiens!</h1>

<div class="container pt-5">

    <div class="row row-cols-1 row-cols-md-2 row-cols-xl-3 g-3">
        <div class="col">
            <div class="card shadow-sm">
                <img src="images/rooms/room1.webp" class="bd-placeholder-img card-img-top" alt="" role="presentation">

                <div class="card-body">
                    <p class="card-text"><strong>Mondrian</strong>, Doppelbettzimmer</p>
                    <p class="card-text">Preis: 133 &euro; / P. / Nacht</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">Buchen</button>
                            <button type="button" class="btn btn-sm btn-outline-primary">Anzeigen</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card shadow-sm">
                <img src="images/rooms/room2.webp" class="bd-placeholder-img card-img-top" alt="" role="presentation">

                <div class="card-body">
                    <p class="card-text"><strong>Flat</strong>, Doppelbettzimmer</p>
                    <p class="card-text">Preis: 123 &euro; / P. / Nacht</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">Buchen</button>
                            <button type="button" class="btn btn-sm btn-outline-primary">Anzeigen</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card shadow-sm">
                <img src="images/rooms/room3.webp" class="bd-placeholder-img card-img-top" alt="" role="presentation">

                <div class="card-body">
                    <p class="card-text"><strong>Oak</strong>, Doppelbettzimmer</p>
                    <p class="card-text">Preis: 143 &euro; / P. / Nacht</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">Buchen</button>
                            <button type="button" class="btn btn-sm btn-outline-primary">Anzeigen</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card shadow-sm">
                <img src="images/rooms/room4.webp" class="bd-placeholder-img card-img-top" alt="" role="presentation">

                <div class="card-body">
                    <p class="card-text"><strong>Futon</strong>, Doppelbettzimmer</p>
                    <p class="card-text">Preis: 113 &euro; / P. / Nacht</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">Buchen</button>
                            <button type="button" class="btn btn-sm btn-outline-primary">Anzeigen</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card shadow-sm">
                <img src="images/rooms/room5.webp" class="bd-placeholder-img card-img-top" alt="" role="presentation">

                <div class="card-body">
                    <p class="card-text"><strong>Dream</strong>, Doppelbettzimmer</p>
                    <p class="card-text">Preis: 153 &euro; / P. / Nacht</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">Buchen</button>
                            <button type="button" class="btn btn-sm btn-outline-primary">Anzeigen</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card shadow-sm">
                <img src="images/rooms/room6.webp" class="bd-placeholder-img card-img-top" alt="" role="presentation">

                <div class="card-body">
                    <p class="card-text"><strong>Emporer</strong>, Suite</p>
                    <p class="card-text">Preis: 173 &euro; / P. / Nacht</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">Buchen</button>
                            <button type="button" class="btn btn-sm btn-outline-primary">Anzeigen</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<?php

include("inc/footer.php")

    ?>