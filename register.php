<?php

$pageTitle = "Registrieren";
$metaDesc = "SEO Meta Description";
include("inc/header.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (!isset($_POST["salutation"]) || $_POST['salutation'] == "") {
        $salutation = false;
        $invalidSalutation = "Bitte wählen Sie eine Anrede";
    } else {
        $salutation = true;
        $salutationValue = $_POST['salutation'];
    }

    if (isset($_POST["firstname"])) {
        $firstname = trim($_POST["firstname"]);
        if (strlen($firstname) == 0) {
            $invalidFirstname = "Bitte geben Sie Ihren Vornamen ein";
        }
    }

    if (isset($_POST["lastname"])) {
        $lastname = trim($_POST["lastname"]);
        if (strlen($lastname) == 0) {
            $invalidLastname = "Bitte geben Sie Ihren Nachnamen ein";
        }
    }

    if (isset($_POST["username"])) {
        $username = trim($_POST["username"]);
        if (strlen($username) == 0) {
            $invalidUsername = "Bitte geben Sie Ihren Usernamen ein";
        }
    }

    if (isset($_POST["email"])) {
        $email = trim($_POST["email"]);
        if (strlen($email) == 0) {
            $invalidEmail = "Bitte geben Sie Ihre E-Mail-Adresse ein";
        } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $invalidEmail = "Bitte überprüfen Sie Ihre E-Mail-Adresse";
        }
    }

    if (isset($_POST["password"])) {
        $password = trim($_POST["password"]);
        if (strlen($password) < 8) {
            $invalidPassword = "Bitte überprüfen Sie die Länge Ihres Passworts";
        } elseif (!preg_match("/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/", $password)) {
            $invalidPassword = "Bitte überprüfen Sie die Anforderungen an die Komplexität Ihres Passworts";
        }
    }

    if (isset($_POST["passwordrepeat"])) {
        $passwordRepeat = trim($_POST["passwordrepeat"]);
        if (strlen($passwordRepeat) < 8) {
            $invalidPasswordRepeat = "Bitte überprüfen Sie die Länge Ihres Passworts";
        } elseif ($password != $passwordRepeat) {
            $invalidPasswordRepeat = "Die Passwörter müssen übereinstimmen";
        }
    }

    if (!isset($_POST["terms"]) || $_POST['terms'] == "") {
        $terms = false;
        $invalidTerms = "Bitte stimmen Sie den Nutzungsbedingungen zu";
    } else {
        $terms = true;
    }

    if (!isset($_POST["privacypolicy"]) || $_POST['privacypolicy'] == "") {
        $privacyPolicy = false;
        $invalidPrivacyPolicy = "Bitte stimmen Sie den Datenschutzbedingungen zu";
    } else {
        $privacyPolicy = true;
    }

}

?>

<h1>Registrieren</h1>

<form method="post" novalidate>

    <div class="mt-3">
        <label class="form-label">Anrede</label>

        <div>
            <div class="form-check form-check-inline">
                <input class="form-check-input <?= !empty($invalidSalutation) ? 'is-invalid' : '' ?>" type="radio"
                    name="salutation" id="salutation1" value="m" <?= !empty($salutationValue) && $salutationValue === "m" ? 'checked' : '' ?> required>
                <label class="form-check-label" for="salutation1">Herr</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input <?= !empty($invalidSalutation) ? 'is-invalid' : '' ?>" type="radio"
                    name="salutation" id="salutation2" value="f" <?= !empty($salutationValue) && $salutationValue === "f" ? 'checked' : '' ?>>
                <label class="form-check-label" for="salutation2">Frau</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input <?= !empty($invalidSalutation) ? 'is-invalid' : '' ?>" type="radio"
                    name="salutation" id="salutation3" value="d" <?= !empty($salutationValue) && $salutationValue === "d" ? 'checked' : '' ?>>
                <label class="form-check-label" for="salutation3">keine</label>
            </div>

            <?= !empty($invalidSalutation) ? '<div class="invalid-feedback d-block">' . $invalidSalutation . '</div>' : '' ?>
            
        </div>

    </div>

    <div class="mt-3">
        <label class="form-label" for="firstname">Vorname</label>

        <input class="form-control <?= !empty($invalidFirstname) ? ' is-invalid' : '' ?>" type="text" id="firstname"
            name="firstname" value="<?= !empty($firstname) ? $firstname : '' ?>" required>

        <?= !empty($invalidFirstname) ? '<div class="invalid-feedback">' . $invalidFirstname . '</div>' : '' ?>

    </div>

    <div class="mt-3">
        <label class="form-label" for="lastname">Nachname</label>

        <input class="form-control <?= !empty($invalidLastname) ? ' is-invalid' : '' ?>" type="text" id="lastname"
            name="lastname" value="<?= !empty($lastname) ? $lastname : '' ?>" required>

        <?= !empty($invalidLastname) ? '<div class="invalid-feedback">' . $invalidLastname . '</div>' : '' ?>

    </div>

    <div class="mt-3">
        <label class="form-label" for="username">Username</label>

        <input class="form-control <?= !empty($invalidUsername) ? ' is-invalid' : '' ?>" type="text" id="username"
            name="username" value="<?= !empty($username) ? $username : '' ?>" required>

        <?= !empty($invalidUsername) ? '<div class="invalid-feedback">' . $invalidUsername . '</div>' : '' ?>

    </div>

    <div class="mt-3">
        <label class="form-label" for="email">E-Mail</label>

        <input class="form-control <?= !empty($invalidEmail) ? ' is-invalid' : '' ?>" type="email" id="email" name="email"
            value="<?= !empty($email) ? $email : '' ?>" required>

        <?= !empty($invalidEmail) ? '<div class="invalid-feedback">' . $invalidEmail . '</div>' : '' ?>

    </div>

    <!-- Quelle für Regex https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a -->
    <div class="mt-3">
        <label class="form-label" for="password">Passwort</label>
        <input class="form-control <?= !empty($invalidPassword) ? 'is-invalid' : '' ?>" type="password" id="password"
            name="password" aria-describedby="passwordhelptext" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"
            minlength="8" value="<?= !empty($password) ? $password : '' ?>" required>
        <?= !empty($invalidPassword) ? '<div class="invalid-feedback">' . $invalidPassword . '</div>' : '' ?>
        <div id="passwordhelptext" class="form-text">Ihr Passwort muss mindestens 8 Zeichen lang sein mit einem
            Buchstaben und einer Ziffer</div>
    </div>

    <div class="mt-3">
        <label class="form-label" for="passwordrepeat">Passwort wiederholen</label>
        <input class="form-control <?= !empty($invalidPasswordRepeat) ? 'is-invalid' : '' ?>" type="password"
            id="passwordrepeat" name="passwordrepeat" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" minlength="8"
            value="<?= !empty($passwordRepeat) ? $passwordRepeat : '' ?>" required>
        <?= !empty($invalidPasswordRepeat) ? '<div class="invalid-feedback">' . $invalidPasswordRepeat . '</div>' : '' ?>
    </div>

    <div class="mt-3 form-check">
        <input class="form-check-input <?= !empty($invalidTerms) ? 'is-invalid' : '' ?>" type="checkbox" id="terms" name="terms"
            <?= !empty($terms) ? 'checked' : '' ?> required>
        <label class="form-check-label" for="terms">Durch meine Nutzung stimme ich den <a
                href="#">Nutzungsbedingungen</a> zu.</label>
        <?= !empty($invalidTerms) ? '<div class="invalid-feedback">' . $invalidTerms . '</div>' : '' ?>
    </div>

    <div class="mt-3 form-check">
        <input class="form-check-input <?= !empty($invalidPrivacyPolicy) ? 'is-invalid' : '' ?>" type="checkbox"
            id="privacypolicy" name="privacypolicy" <?= !empty($privacyPolicy) ? 'checked' : '' ?> required>
        <label class="form-check-label" for="privacypolicy">Ich stimme der Verarbeitung meiner Daten im Sinne der <a
                href="#">Datenschutzerklärung</a> zu.</label>
        <?= !empty($invalidPrivacyPolicy) ? '<div class="invalid-feedback">' . $invalidPrivacyPolicy . '</div>' : '' ?>
    </div>

    <div class="mt-3">
        <input class="btn btn-primary" type="submit" value="Registrieren">
    </div>

</form>

<p class="mt-3">Sie haben bereits Benutzerkonto? <a href="login.php">Jetzt einloggen!</a></p>

<?php

include("inc/footer.php")

    ?>