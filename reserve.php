<?php

    $pageTitle = "Zimmer reservieren";
    $metaDesc = "SEO Meta Description";
    include("inc/header.php");

?>

<h1>Zimmer reservieren</h1>

<!-- 

Registrierte User*innen können Zimmer reservieren
und folgende Leistungen optional mit Aufpreis dazu-buchen:
o Frühstück
o Parkplatz
o Mitnahme von Haustieren
Die Höhe der jeweiligen Aufpreise kann vom Projektteam frei (jedoch 
sinnvoll) selbst gewählt werden.
Für die Reservierung der einzelnen Zimmer ist auch deren zeitliche 
Verfügbarkeit zu berücksichtigen!

-->

<?php

    include("inc/footer.php")

?>