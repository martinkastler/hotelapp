<?php

$pageTitle = "News";
$metaDesc = "SEO Meta Description";
include("inc/header.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $invalidNewstitle = $invalidNewscontent = $invalidNewsthumbnail = false;

    if (isset($_POST["newstitle"])) {
        $newstitle = $_POST["newstitle"];
        if (strlen($newstitle) == 0) {
            $invalidNewstitle = "Bitte geben Sie einen Newstitel ein";
        }
    }

    if (isset($_POST["newscontent"])) {
        $newscontent = $_POST["newscontent"];
        if (strlen($newscontent) == 0) {
            $invalidNewscontent = "Bitte geben Sie einen Newsinhalt an";
        }
    }

    // Check write permissions on the tmp folder!
    if (is_uploaded_file($_FILES["newsthumbnail"]["tmp_name"])) {
        $tempFile = $_FILES["newsthumbnail"]["tmp_name"];
        $targetDirectory = __DIR__ . "/uploads/news/";
        $targetFile = $targetDirectory.basename($_FILES["newsthumbnail"]["name"]);

        // TODO Check for empty file
        // TODO Check for filesize
        // TODO Check for filetype
        // TODO Resize image, e.g. imagescale()

        if (!move_uploaded_file($tempFile, $targetFile)) {
            $invalidNewsthumbnail = "Datei konnte nicht hochgeladen werden";
        }

    }

    if (!$invalidNewstitle && !$invalidNewscontent && !$invalidNewsthumbnail) {
        header("Location: news.php");
        exit();
    }

}

?>

<h1>News</h1>

<!-- Administrator*innen können News-Beiträge posten inkl. Bildupload. -->

<div class="p-3 mt-3 rounded border">
    <a class="btn btn-secondary" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false"
        aria-controls="collapseExample">
        Newsbeitrag posten
    </a>

    <div class="collapse <?= $invalidNewstitle || $invalidNewscontent ? 'show' : '' ?>" id="collapseExample">

        <form method="post" enctype="multipart/form-data">

            <div class="mt-3">
                <input class="form-control <?= !empty($invalidNewstitle) ? 'is-invalid' : '' ?>" type="text"
                    id="newstitle" name="newstitle" placeholder="Newstitel"
                    value="<?= !empty($newstitle) ? $newstitle : '' ?>" required>
                <?= !empty($invalidNewstitle) ? '<div class="invalid-feedback is-invalid">' . $invalidNewstitle . '</div>' : '' ?>
            </div>

            <div class="mt-3">
                <textarea class="form-control <?= !empty($invalidNewscontent) ? 'is-invalid' : '' ?>" id="newscontent"
                    name="newscontent" rows="3"
                    placeholder="Newsinhalt hier eingeben ..." required><?= !empty($newscontent) ? $newscontent : '' ?></textarea>
                    <?= !empty($invalidNewscontent) ? '<div class="invalid-feedback is-invalid">' . $invalidNewscontent . '</div>' : '' ?>
            </div>

            <div class="mt-3">
                <label for="newsthumbnail" class="form-label">Newsbild hochladen</label>
                <input class="form-control <?= !empty($invalidNewsthumbnail) ? 'is-invalid' : '' ?>"
                    aria-describedby="newsthumbnailhelptext" type="file" id="newsthumbnail" name="newsthumbnail">
                <div id="newsthumbnailhelptext" accept="image/jpeg,image/png" class="form-text">Formate: JPG, PNG. Maximale Dateigröße: 3 MB</div>
                <?= !empty($invalidNewsthumbnail) ? '<div class="invalid-feedback is-invalid">' . $invalidNewsthumbnail . '</div>' : '' ?>
            </div>

            <div class="mt-3">
                <input class="btn btn-primary" type="submit" value="Posten">
            </div>

        </form>
    </div>
</div>

<div class="d-md-flex bg-light p-3 mt-3 rounded shadow-sm">
    <div class="flex-shrink-0 text-center mb-3 mb-md-0 me-3">
        <img src="images/hotelnews-placeholder.jpg" class="img-fluid img-thumbnail" alt="" role="presentation">
    </div>
    <div class="flex-grow-1">
        <h2>News headline 1</h2>
        <p class="lead">This example is a quick exercise to illustrate how the top-aligned navbar works. As you scroll,
            this
            navbar remains in its original position and moves with the rest of the page.</p>
        <small class="text-muted">2024-01-01</small>
    </div>
</div>

<div class="d-md-flex bg-light p-3 mt-3 rounded shadow-sm">
    <div class="flex-shrink-0 text-center mb-3 mb-md-0 me-3">
        <img src="images/hotelnews-placeholder.jpg" class="img-fluid img-thumbnail" alt="" role="presentation">
    </div>
    <div class="flex-grow-1">
        <h2>News headline 2</h2>
        <p class="lead">This example is a quick exercise to illustrate how the top-aligned navbar works. As you scroll,
            this
            navbar remains in its original position and moves with the rest of the page.</p>
        <small class="text-muted">2024-01-01</small>
    </div>
</div>

<div class="d-md-flex bg-light p-3 mt-3 rounded shadow-sm">
    <div class="flex-shrink-0">
    </div>
    <div class="flex-grow-1">
        <h2>News headline 3 - no image</h2>
        <p class="lead">This example is a quick exercise to illustrate how the top-aligned navbar works. As you scroll,
            this
            navbar remains in its original position and moves with the rest of the page.</p>
        <small class="text-muted">2024-01-01</small>
    </div>
</div>

<?php

include("inc/footer.php")

    ?>