<?php

    session_start();

?>
<!doctype html>
<html lang="de" <?= !empty($_COOKIE["theme"]) && $_COOKIE["theme"] == 'dark' ? 'data-bs-theme="dark"' : '' ?> >

<head>
    <title>
        <?= $pageTitle ?> | Hotel.ly
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= $metaDesc ?>">
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" href="images/favicon.png">
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body class="pt-5">

    <header>
        <nav class="navbar navbar-light fixed-top navbar-expand-md shadow" aria-label="Hauptnavigation">
            <div class="container">
                <a class="navbar-brand" href="index.php" aria-label="Zur Startseite zurückkehren">
                    <img src="images/favicon.png" alt="" role="presentation" width="20" height="20"
                        class="d-inline-block align-text-top mt-1">
                    Hotel.ly
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false"
                    aria-label="Navigation umschalten">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsExample03">
                    <ul class="navbar-nav me-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Zimmer &amp; Preise</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="news.php">News</a>
                        </li>
                    </ul>

                    <!-- if logged in -->
                    <?php if(!empty($_SESSION["active"])): ?>

                        <!-- if admin -->
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                    Administration
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="reservations.php">Reservierungen</a></li>
                                    <li><a class="dropdown-item" href="users.php">Benutzerverwaltung</a></li>
                                    <li><a class="dropdown-item" href="news.php">Newsverwaltung</a></li>
                                </ul>
                            </li>
                        </ul>
                        <!-- /if admin in -->

                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                    Willkommen, <?= $_SESSION["firstname"] ?>!
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="profile.php">Mein Profil</a></li>
                                    <li><a class="dropdown-item" href="my-reservations.php">Meine Reservierungen</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                        <!-- /if logged in -->

                    <?php else: ?>
                    
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="btn btn-link" href="register.php">Registrieren</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-secondary" href="login.php">Login</a>
                            </li>
                        </ul>

                    <?php endif; ?>

                </div>    
            </div>
        </nav>
    </header>

    <main class="container mt-5">