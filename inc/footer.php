</main>

<footer class="container mt-5 border-top">
    <nav class="nav" aria-label="Footer-Navigation">
        <a class="nav-link text-muted disabled" aria-hidden="true">&copy; 2023 Hotel.ly</a>
        <a class="nav-link text-muted text-decoration-underline" href="impressum.php">Impressum</a>
        <a class="nav-link text-muted text-decoration-underline" href="hilfe.php">Hilfe</a>
    </nav>
</footer>
</body>

</html>