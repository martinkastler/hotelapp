<?php

$pageTitle = "Login";
$metaDesc = "SEO Meta Description";
include("inc/header.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $invalidEmail = $invalidPassword = false;

    if (isset($_POST["email"])) {
        $email = $_POST["email"];
        if (strlen($email) == 0) {
            $invalidEmail = "Bitte geben Sie Ihre E-Mail-Adresse ein";
        } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $invalidEmail = "Bitte überprüfen Sie Ihre E-Mail-Adresse";
        }
    }
    
    if (isset($_POST["password"])) {
        $password = $_POST["password"];
        if (strlen($password) < 8) {
            $invalidPassword = "Bitte überprüfen Sie die Länge Ihres Passworts";
        }
    }

    if (!$invalidEmail && !$invalidPassword) {
        $_SESSION["active"] = true;
        $_SESSION["firstname"] = "Martin";
        setcookie("theme", "dark", time() + 3600); // 1 hour
        header("Location: index.php");
        exit();
    }

}

?>

<h1>Login</h1>

<form method="post">

    <div class="mt-3">
        <label class="form-label" for="email">E-Mail</label>

        <input class="form-control <?= !empty($invalidEmail) ? 'is-invalid' : '' ?>" type="email" id="email" name="email"
            value="<?= !empty($email) ? $email : '' ?>" required>

        <?= !empty($invalidEmail) ? '<div class="invalid-feedback is-invalid">' . $invalidEmail . '</div>' : '' ?>

    </div>

    <div class="mt-3">
        <label class="form-label" for="password">Passwort</label>

        <input class="form-control
                <?= !empty($invalidPassword) ? 'is-invalid' : '' ?>" type="password" id="password" name="password"
            minlength="8" value="<?= !empty($password) ? $password : '' ?>" required>

        <?= !empty($invalidPassword) ? '<div class="invalid-feedback is-invalid">' . $invalidPassword . '</div>' : '' ?>

    </div>

    <div class="mt-3">
        <input class="btn btn-primary" type="submit" value="Einloggen">
    </div>

</form>

<p class="mt-3">Haben Sie noch kein Benutzerkonto? <a href="register.php">Jetzt registrieren!</a></p>

<?php

include("inc/footer.php")

    ?>